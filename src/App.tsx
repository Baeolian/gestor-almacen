import * as React from 'react';
import './assets/scss/main.scss';
import Header from './components/Header';
import Body from './components/Body';
import Footer from './components/Footer';

class App extends React.Component {
  public render() {
    return(
      <div className="App">
        <Header logged={true} />
        <Body />
        <Footer empresa={'Universidad de Sevilla'} year={'2021'} />
      </div>
    );
  }
}

export default App;
