import * as React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSolarPanel, faTools, faLaptop, faClipboard } from '@fortawesome/free-solid-svg-icons';

library.add(faSolarPanel, faTools, faLaptop, faClipboard);

class Body extends React.Component {
    public render (){
        return (
            <div className="body row">
                <p>
                    <a className="materiales-btn btn btn-success col-md-3" href="https://www.google.es/">
                    <FontAwesomeIcon icon="solar-panel" />
                    </a>
                    <a className="herramientas-btn btn btn-success col-md-3" href="https://www.google.es/">
                        <FontAwesomeIcon icon="tools" />
                    </a>
                    <a className="informatica-btn btn btn-success col-md-3" href="https://www.google.es/">
                        <FontAwesomeIcon icon="laptop" />
                    </a>
                    <a className="registro-btn btn btn-warning col-md-3" href="https://www.google.es/">
                        <FontAwesomeIcon icon="clipboard" />
                    </a>
                </p>
            </div>
        );
    }
}

export default Body;
