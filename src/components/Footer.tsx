import * as React from 'react';

interface IFooterProps {
    empresa: string;
    year: string;
}

class Footer extends React.Component<IFooterProps> {
    public render (){
        return (
            <div className="footer">
                <p className="text-left">{this.props.empresa} © {this.props.year}</p>
            </div>
        );
    }
}

export default Footer;
