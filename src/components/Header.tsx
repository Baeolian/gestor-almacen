import * as React from 'react';
import logo from '../us.jpg'

interface IHeaderProps {
    logged: boolean;
}

interface IHeaderState {
    logged: boolean;
}

class Header extends React.Component<IHeaderProps, IHeaderState> {
    constructor (props: IHeaderProps){
        super(props);
        this.state = {logged: false}
    }

    public changeState = () => {
        this.setState({logged: !!!this.state.logged});
    }

    public render (){
        return (
            <div className="position-relative">
                <p>
                    <img id="logoUS" src={logo} />
                    <a id="loginState">{this.state.logged ? "Administrador":""}</a>
                    <a className="login-btn btn btn-primary" onClick={this.changeState}>
                        {this.state.logged ? "Logout":"Login"}
                    </a>
                </p>
            </div>
        );
    }
}

export default Header;
