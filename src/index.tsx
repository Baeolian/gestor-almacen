import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './assets/css/main.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);

reportWebVitals();
